const io = require('socket.io-client');

export default function () {
  // const socket = io.connect('https://zpi-battleship-server.herokuapp.com');
  const socket = io.connect('http://localhost:3001');

  function registerHandler(onMessageReceived) {
    socket.on('message', onMessageReceived);
  }

  function registerGameFinished(onGameFinished) {
    socket.on('gameFinished', onGameFinished);
  }

  function unregisterGameFinished() {
    socket.on('gameFinished');
  }

  function registerGameReadyHandler(onGameReady) {
    socket.on('gameReady', onGameReady);
  }

  function unregisterGameReadyHandler() {
    socket.off('gameReady');
  }

  function newUser(user, cb) {
    socket.emit('newuser', user, cb);
  }

  function createRoom(name, cb) {
    socket.emit('createroom', name, cb);
  }

  function unregisterHandler() {
    socket.off('message');
  }

  socket.on('error', (err) => {
    console.log('received socket error:', err);
  });

  function register(name, cb) {
    socket.emit('register', name, cb);
  }

  function join(roomName, cb) {
    socket.emit('join', roomName, cb);
  }

  function leave(roomName, cb) {
    socket.emit('leave', roomName, cb);
  }

  function message(roomName, msg, cb) {
    socket.emit('message', { roomName, message: msg }, cb);
  }

  function fireTorpedo(roomName, [row, col], cb) {
    socket.emit('fireTorpedo', { roomName, cords: [row, col] }, cb);
  }

  function getRooms(cb) {
    socket.emit('rooms', null, cb);
  }

  function registerRoomsUpdate(cb) {
    socket.on('roomsUpdated', cb);
  }

  function unregisterRoomsUpdate() {
    socket.off('roomsUpdated');
  }

  function registerYourTurn(cb) {
    socket.on('yourTurn', cb);
  }

  function unregisterYourTurn() {
    socket.off('yourTurn');
  }

  function getAvailableUsers(cb) {
    socket.emit('availableUsers', null, cb);
  }

  function setUserBoard(roomName, board, cb) {
    socket.emit('setUserBoard', { roomName, board }, cb);
  }

  return {
    register,
    join,
    leave,
    message,
    getRooms,
    getAvailableUsers,
    registerHandler,
    unregisterHandler,
    newUser,
    createRoom,
    setUserBoard,
    registerGameReadyHandler,
    unregisterGameReadyHandler,
    fireTorpedo,
    registerRoomsUpdate,
    unregisterRoomsUpdate,
    registerYourTurn,
    unregisterYourTurn,
    registerGameFinished,
    unregisterGameFinished,
  };
}
