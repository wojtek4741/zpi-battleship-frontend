import axios from 'axios';

console.log(process.env.NODE_ENV);
const apiProvider = axios.create({
  baseURL: 'http://localhost:3008',
  // baseURL: 'https://zpi-battleship-api.herokuapp.com',
  timeout: 180000,
});

export function getUsers() {
  return apiProvider.get('/users');
}

export function getUser(email) {
  return apiProvider.get(`/user/?email=${email}`);
}

export default function () {
  return null;
}

export function getUserBattles(email) {
  return apiProvider.get(`/user/battles/?email=${email}`);
}
