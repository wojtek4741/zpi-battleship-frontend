import React, { Component } from 'react';

export default class NewUser extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      value: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit() {
    if (this.state.value !== '') {
      console.log('this.state.value:', this.state.value);
      this.props.register(this.state.value);
    }
  }

  render() {
    return (
      <div style={{ marginTop: 30 }}>
        Podaj swoją nazwę uytkownika
        <input
          type="text"
          onChange={this.handleChange}
          value={this.state.value}
          onKeyPress={(e) => (e.key === 'Enter' ? this.handleSubmit() : null)}
        />
        <div onClick={this.handleSubmit}>gotowe</div>
      </div>
    );
  }
}
