/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Component } from 'react';
import { getUsers, getUser, getUserBattles } from '../networking';
import loader from '../assets/loader.svg';

export default class Ranking extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      isLoading: true,
      users: [],
      searchedUser: '',
      searchedUserData: {},
      userBattles: [],
    };

    this.getUsers = this.getUsers.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  getUsers() {
    getUsers().then((result) => {
      console.log('result: ', result.data);
      this.setState({ users: result.data, isLoading: false });
    }).catch((error) => {
      console.log(error);
    });
  }

  componentDidMount() {
    this.getUsers();
  }

  handleChange(event) {
    this.setState({ searchedUser: event.target.value });
  }

  handleSubmit(event) {
    this.getUserData();
    event.preventDefault();
  }

  findUser(email) {
    this.setState({ searchedUser: email }, () => this.getUserData());
  }

  getUserData() {
    this.setState({ isLoading: true });
    getUser(this.state.searchedUser).then((result) => {
      this.setState({ searchedUserData: result.data });
      getUserBattles(this.state.searchedUser).then((battles) => {
        console.log('battles: ', battles.data);
        this.setState({ userBattles: battles.data, isLoading: false });
      });
    }).catch((error) => {
      console.log(error);
    });
  }


  render() {
    const { isLoading, searchedUser, searchedUserData, userBattles } = this.state;
    return (
      <div className="fixed z-2 top-0 left-0 w-100 h-100" style={{ backgroundColor: '#3D3D3D' }}>
        <div id="header" className="bg-2 pa3 flex items-center shadow-5 z-1">
          <form onSubmit={this.handleSubmit} className="w-30">
            <label>
              Wyszukaj gracza:
              <input type="text" value={searchedUser} onChange={this.handleChange} />
            </label>
            <input type="submit" value="Szukaj" />
          </form>
          <h2 className="w-40 tc">RANKING</h2>
          <div className="flex items-center justify-end pa2 w-30">
            <div className="ma2 pointer" onClick={() => this.props.closeRanking()}>Zamknij</div>
          </div>
        </div>
        <div className="flex flex-column pa3 mh3 center tl">
          {Object.keys(searchedUserData).length > 0
            ? (
              <>
                <div className="flex items-start pa3 mh3 center tl w-100 tc">
                  <div className="pa3 br3 bg-black-30">
                    <h3 className="tl mt0">
                      {`${searchedUserData.email} (${searchedUserData.points})`}
                    </h3>
                    <p className="ma0 tl">{`Wygrane: ${searchedUserData.wins}`}</p>
                    <p className="ma0 tl">{`Pojedynki: ${searchedUserData.battles}`}</p>
                  </div>
                  <div className="flex flex-auto flex-column ph3">
                    <h3 className="mb2 mt0 tl">Historia meczy</h3>
                    <table>
                      <thead>
                        <tr>
                          <td className="tc">Gracz</td>
                          <td className="tc">Punkty</td>
                          <td className="tc">Wynik</td>
                          <td className="tc">Trafienia</td>
                          <td className="tc">Strzały ogółem</td>
                        </tr>
                      </thead>
                      <tbody>
                        {userBattles.map((battle) => (
                          <>
                            <p className="tl mb0">{battle.id_date}</p>
                            <tr className="bg-green white">
                              <td className="tc pointer" onClick={() => this.findUser(battle.winner)}>{'winner' in battle ? battle.winner : ''}</td>
                              <td className="tc">{'winner_rank' in battle ? battle.winner_rank : ''}</td>
                              <td className="tc">{'winner_points' in battle ? battle.winner_points : ''}</td>
                              <td className="tc">{'winner_points' in battle ? battle.winner_hits : ''}</td>
                              <td className="tc">{'winner_shots' in battle ? battle.winner_shots : ''}</td>
                            </tr>
                            <tr className="white" style={{ backgroundColor: '#ff695e' }}>
                              <td className="tc pointer" onClick={() => this.findUser(battle.looser)}>{'looser' in battle ? battle.looser : ''}</td>
                              <td className="tc">{'looser_rank' in battle ? battle.looser_rank : ''}</td>
                              <td className="tc">{'looser_points' in battle ? battle.looser_points : ''}</td>
                              <td className="tc">{'winner_points' in battle ? battle.looser_hits : ''}</td>
                              <td className="tc">{'looser_shots' in battle ? battle.looser_shots : ''}</td>
                            </tr>
                          </>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="blue mt3 pointer center" onClick={() => this.setState({ searchedUser: '', searchedUserData: {} })}>wyświetl wszystkich</div>
              </>
            ) : (
              <div className="flex flex-column pa3 mh3 center tl w-100">
                <h3 className="center">Ranking ogólny</h3>
                <table>
                  <thead>
                    <tr>
                      <td className="tc">Pozycja</td>
                      <td className="tc">Gracz</td>
                      <td className="tc">Punkty</td>
                      <td className="tc">Wygrane</td>
                      <td className="tc">Pojedynki</td>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.users.map((u, i) => (
                      <tr key={i}>
                        <td className="tc">{`${i + 1}.`}</td>
                        <td className="tc pointer" onClick={() => this.findUser(u.email)}>{u.email}</td>
                        <td className="tc">{u.points}</td>
                        <td className="tc">{u.wins}</td>
                        <td className="tc">{u.battles}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            )}
          {isLoading ? (
            <img src={loader} style={{ width: 100, height: 100 }} className="center" alt="loader" />
          ) : null}
        </div>
      </div>
    );
  }
}
