import React, { Component } from 'react';

export default class CreateRoom extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      value: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onRoomCreated = this.onRoomCreated.bind(this);
  }

  onRoomCreated() {
    this.props.getRooms();
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit() {
    if (this.state.value !== '') {
      console.log('this.state.value:', this.state.value);
      this.props.createRoom(this.state.value);
    }
  }

  render() {
    return (
      <div className="br3 pa3 bg-white-10 flex flex-column">
        <p style={{ fontSize: 12 }} className="ma0 pa0 fw7">{'Nowy pokój'.toUpperCase()}</p>
        <input
          type="text"
          onChange={this.handleChange}
          value={this.state.value}
          style={{ borderRadius: 5, width: '70%' }}
          className="center mt3"
          onKeyPress={(e) => (e.key === 'Enter' ? this.handleSubmit() : null)}
        />
        <div onClick={this.handleSubmit} className="mt4 center br-pill bg-green pv2 ph3">stwórz</div>
      </div>
    );
  }
}
