module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    "import/no-named-as-default": 0,
    camelcase: 0,
    "react/destructuring-assignment": 0,
    "no-console": 0,
    "max-len": 0,
    "react/jsx-filename-extension": 0,
    "jsx-a11y/click-events-have-key-events": 0,
    "jsx-a11y/no-static-element-interactions": 0,
    "react/prop-types": 0,
    "no-alert": 0,
    "no-plusplus": 0,
    "react/no-array-index-key": 0,
    "react/sort-comp": 0,
    "linebreak-style": 0,
    "object-curly-newline": 0
  },
};
